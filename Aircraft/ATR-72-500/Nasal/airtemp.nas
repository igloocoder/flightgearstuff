var staticTemp = props.globals.getNode("environment/temperature-degc", 1);
var machNumber = props.globals.getNode("velocities/mach", 0);

var totalAirTemp = func
 {
 setprop("environment/temperature-total-degc", (1 + ((1.4-1)/2*machNumber.getValue()))*staticTemp.getValue()); 
 settimer(totalAirTemp, 2);
 }
 
setlistener("/sim/signals/fdm-initialized", totalAirTemp);